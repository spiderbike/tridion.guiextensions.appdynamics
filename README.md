# Tridion GUI Extension for Application Dynamics

This Gui Extension adds in the Application Dynamics adrum javascript in to the CME for Application Dynamics reporting
 

## Getting Started

These instructions will get the GUI Extension installed on your Tridion CME Server

### Prerequisites

You will need at least Tridion 8.0

* [SDL Tridion](https://www.sdl.com/tridion) - SDL Tridion
```
An Application Insights Account
An AppKey for Application Dynamics
```


### Installing

A step by step of how to install in Tridion CME

* Download this repository locally
* Copy the files to a folder on the CME
* In IIS Add a new virtual directory inside the Tridion CME site under WebUI\Editors and fill in the folder and point it to the folder you put the downloaded files in, and set the alias to "AppD"
* Open the D:\Apps\SDL Web\web\WebUI\WebRoot\Configuration\System.config 
* Just before the closing < /editors> tag add the following (replacing file path with the folder you used.)
```xml
	
<editor name="AppD">
    <installpath>D:\Apps\GuiExtensions\AppD\</installpath>
    <configuration>Configuration\AppD.config</configuration>
    <vdir>AppD</vdir>
</editor>
```
* In the same file increment the  modification attribute by 1

```Note: Don`t forget to update the App Key in AppD.js ```





### Running the Extension

* Login to the CME
* You should see the requests going out to AppD



### And coding style tests

Coding guidlines, are default Resharper recomendations.


## Built With


* [Visual Studio](https://www.microsoft.com/visualstudio/) - VS2017

## Contributing

Please follow normal Git flow branching and pull request strategy

## Versioning

no tagging strategy defined

## Authors

* **Mark Richardson** - *Initial work* - [Tridion Ted](https://twitter.com/tridionted)


## License

This project is open source, feel free to modify or share for free or for proffit.

## Acknowledgments

* Stack exchange