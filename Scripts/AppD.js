console.log("AppD Running");
window['adrum-start-time'] = new Date().getTime();
(function(config){
	console.log("Have you updated the AppD appKey in AppD.js?");
	//You`ll need to populate this with your AppD App Key
    config.appKey = 'XX-XXX-XXX-XXX';
    config.adrumExtUrlHttp = 'http://cdn.appdynamics.com';
    config.adrumExtUrlHttps = 'https://cdn.appdynamics.com';
    config.beaconUrlHttp = 'http://col.eum-appdynamics.com';
    config.beaconUrlHttps = 'https://col.eum-appdynamics.com';
    config.xd = {enable : true};
})(window['adrum-config'] || (window['adrum-config'] = {}));
